import softest

import logging
import inspect

from openpyxl import Workbook, load_workbook
import csv

class Utils(softest.TestCase):
    def assert_list_item_text(self, list, value):
        for stop in list:
            print("The text is: " + stop.text)
            self.soft_assert(self.assertEqual, stop.text, value)
            if stop.text == value:
                print("test passed")
            else:
                print("test failed")
        self.assert_all()


    def custom_logger(log_level=logging.DEBUG):
        # Set calss/method name from where its called
        logger_name = inspect.stack()[1][3]
        # create logger
        logger = logging.getLogger(logger_name)
        logger.setLevel(log_level)
        # create console handler or file handler and set the log level
        fh = logging.FileHandler("automation.log")
        # create formatter - how you want your logs to be formatted
        formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(name)s : %(message)s", datefmt='%d.%m.%Y %H:%M:%S')
        # add formatter to console or file handler
        fh.setFormatter(formatter)
        # add console handler to logger
        logger.addHandler(fh)
        return logger
    
    def read_data_from_excel(file_name, sheet):
        data_list = []
        wb = load_workbook(filename=file_name)
        sh = wb[sheet]
        row_ct = sh.max_row
        col_ct = sh.max_column
        for i in range(2, row_ct + 1):
            row = []
            for j in range(1, col_ct + 1):
                row.append(sh.cell(row=i, column=j).value)
            data_list.append(row)
        return data_list

    def read_data_from_csv(file_name):
        data_list = []
        csvdata = open(file_name, "r")
        reader = csv.reader(csvdata)
        next(reader)    # skip header string
        for row in reader:
            data_list.append(row)
        return data_list
        