import pytest
from pages.yatra_launch_page import LaunchPage
from utilities.utils import Utils
import softest
import logging
from ddt import ddt, data, unpack, file_data



@pytest.mark.usefixtures("setup")
@ddt
class TestSearchgAndVerifyFilter(softest.TestCase):
    log = Utils.custom_logger(log_level=logging.INFO)

    @pytest.fixture(autouse=True)
    def class_setup(self):
        self.lp = LaunchPage(self.driver)
        self.ut = Utils()

    # @data(("Mumbai", "New York", "20/09/2023", "1 Stop"), ("Bangalore", "Chennai", "21/09/2023", "2 Stop"))
    # @unpack
    # @file_data("../testdata/testdata.json")
    # @file_data("../testdata/testyam.yaml")
    # @data(*Utils.read_data_from_excel("C:\\Users\\PaulKP\\Documents\\Projects\\python\\selenium\\python_test_framework\\testdata\\tdataexcel.xlsx", "Аркуш1"))
    @data(*Utils.read_data_from_csv("C:\\Users\\PaulKP\\Documents\\Projects\\python\\selenium\\python_test_framework\\testdata\\tdatacsv.csv"))
    @unpack
    def test_search_flights_1_stop(self, going_from, going_to, date, stops):
        # self.lp.close_spam_iframe()
        search_flights_result = self.lp.search_flights(
            going_from, going_to, date)   
        self.lp.page_scroll()
        search_flights_result.filter_flights_by_stop(stops)
        allstops1 = search_flights_result.get_search_flight_results()
        self.log.info(len(allstops1))  
        self.ut.assert_list_item_text(allstops1, stops)
    
    # def test_search_flights_2_stop(self):
    #     search_flights_result = self.lp.search_flights(
    #         "New Delhi", "JFK", "16/09/2023")     
    #     self.lp.page_scroll()
    #     search_flights_result.filter_flights_by_stop("2 Stop")
    #     allstops1 = search_flights_result.get_search_flight_results()
    #     print(len(allstops1))  
    #     self.ut.assert_list_item_text(allstops1, "2 Stop")
