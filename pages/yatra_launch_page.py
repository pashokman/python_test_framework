import time
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from base.base_driver import BaseDriver
from pages.search_flight_results_page import SearchFlightResults

from utilities.utils import Utils
import logging


class LaunchPage(BaseDriver):
    log = Utils.custom_logger()

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    # Locators
    DEPART_FROM_FIELD = "//input[@id='BE_flight_origin_city']"
    DEPART_FROM_LIST = ".origin_ac li"
    GOING_TO_FIELD = "//input[@id='BE_flight_arrival_city']"
    GOING_TO_LIST = ".dest_ac li"
    DEPART_DATE_FIELD = "BE_flight_origin_date"
    DEPART_DATE_LIST = "//div[@id='monthWrapper']//td[@class!='inActiveTD']"
    SEARCH_BUTTON = "BE_flight_flsearch_btn"
    SPAM_FRAME = "//iframe[@id='webklipper-publisher-widget-container-notification-frame']"
    SPAM_CLOSE_BUTTON = "//a[@id='webklipper-publisher-widget-container-notification-close-div']"

    #Provide iframe closing
    def get_spam_iframe(self):
        return self.wait_until_presence_of_element_located(By.XPATH, self.SPAM_FRAME)
    
    def enter_into_spam_iframe(self):
        return self.driver.switch_to.frame(self.driver.find_element(By.XPATH, self.SPAM_FRAME))
    
    def get_close_spam_iframe_btn(self):
        return self.wait_until_element_is_clicable(By.XPATH, self.SPAM_CLOSE_BUTTON)
    
    def back_into_main_iframe(self):
        return self.driver.switch_to.default_content()
    
    def close_spam_iframe(self):
        self.get_spam_iframe()
        self.enter_into_spam_iframe()
        self.get_close_spam_iframe_btn().click()
        self.back_into_main_iframe()

    # Provide depart from location
    def get_depart_from_field_location(self):
        return self.wait_until_element_is_clicable(By.XPATH, self.DEPART_FROM_FIELD)

    def get_depart_from_list(self):
        return self.wait_for_presence_of_all_elements(
            By.CSS_SELECTOR, self.DEPART_FROM_LIST
        )

    def check_depart_from_value(self, depart_location):
        return self.wait_until_text_to_be_present_in_element_value(
            By.XPATH, self.DEPART_FROM_FIELD, depart_location
        )

    def enter_depart_from_location(self, depart_location):
        self.get_depart_from_field_location().click()
        self.get_depart_from_list()
        self.get_depart_from_field_location().send_keys(depart_location)
        self.check_depart_from_value(depart_location)
        self.get_depart_from_field_location().send_keys(Keys.ENTER)

    # Provide going to location
    def get_going_to_field_location(self):
        return self.wait_until_element_is_clicable(By.XPATH, self.GOING_TO_FIELD)

    def get_going_to_list(self):
        return self.wait_for_presence_of_all_elements(
            By.CSS_SELECTOR, self.GOING_TO_LIST
        )

    def check_going_to_value(self, going_to):
        return self.wait_until_text_to_be_present_in_element_value(
            By.XPATH, self.GOING_TO_FIELD, going_to
        )

    def enter_going_to_location(self, going_to):
        self.get_going_to_field_location().click()
        self.log.info("Clicked on going to")
        self.get_going_to_list()
        self.get_going_to_field_location().send_keys(going_to)
        self.log.info("Typed text into going to field successfully")
        self.check_going_to_value(going_to)
        self.get_going_to_field_location().send_keys(Keys.ENTER)

    # Select departure date
    def get_depart_date_field_location(self):
        return self.wait_until_element_is_clicable(
            By.CLASS_NAME, self.DEPART_DATE_FIELD
        )

    def get_depart_dates_list(self):
        return self.wait_for_presence_of_all_elements(By.XPATH, self.DEPART_DATE_LIST)

    def check_depart_date(self, depart_date):
        return self.wait_until_text_to_be_present_in_element_value(
            By.CLASS_NAME, self.DEPART_DATE_FIELD, depart_date
        )

    def enter_depart_date(self, depart_date):
        self.get_depart_date_field_location().click()
        dates = self.get_depart_dates_list()
        for date in dates:
            if date.get_attribute("data-date") == depart_date:
                date.click()
                break
        self.check_depart_date(depart_date)

    # Click on flight search button
    def get_search_button(self):
        return self.wait_until_element_is_clicable(By.ID, self.SEARCH_BUTTON)

    def click_search_button(self):
        self.get_search_button().click()
        time.sleep(4)

    # Call all page methods in one method and create a next page object
    def search_flights(self, depart_location, going_to_location, depart_date):
        self.enter_depart_from_location(depart_location)
        self.enter_going_to_location(going_to_location)
        self.enter_depart_date(depart_date)
        self.click_search_button()
        search_flights_result = SearchFlightResults(self.driver)
        return search_flights_result
